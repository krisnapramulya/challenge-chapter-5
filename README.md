# Challenge-Chapter-5

- Tempat Penyimpanan File Challenge Chapter 5
- Nama  : Krisna Pramulya Putra
- Kelas : FSW-1


Link DB Diagram : (https://dbdiagram.io/d/62657d0f1072ae0b6adc0000)

Features :

- Create new car data
- Read list of cars
- Update car data
- Delete car data



| How to Run (Installation) | ------ |
| ------ | ------ |
| 1. Initialization | npx sequelize-cli init |
| 2. Change the config.json | change username,password,database,host,dialect in development at config.json |
| 3. Create Database | npx sequelize-cli db:create|
| 4. Create table Cars | npx sequelize model:generate --name cars --attributes name:string,price:integer,size:string,photo:text |
| 5. Migrate| npx sequelize-cli db:migrate|




-Table cars
- {id bigint [pk, increment]// auto-increment, 
- name varchar, 
- price integer, 
- size varchar, 
- photo text, 
- create_at timestamp, 
- updated_at timestamp }


-endpoint crud

- app.post("/cars", carsController.createCars);
- app.post("/cars/:id", carsController.updateCarsById);
- app.post("/deleteCars/:id", carsController.deleteCarsById);


-define endpoint ejs


1. app.get("/", async (req, res) => {
  const getCars = await carsController.getCars();
  res.render("index", {
    cars: getCars,
  });
});

2. app.get("/cars", (req, res) => {
  res.render("create");
});

3. app.get("/update/:id", carsController.renderCarById);


